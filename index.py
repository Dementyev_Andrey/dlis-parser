from dlisio import dlis
import pandas as pd


file_name = input()
with dlis.load(file_name) as files:
    for f in files:
        for frame in f.frames:
            curves = frame.curves()
            dataset = pd.DataFrame(curves)
            dataset.to_csv("{}.csv".format(file_name), sep=',', encoding='utf-8', index=False)
            break
        quit()



